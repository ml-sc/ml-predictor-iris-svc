from predict import predict


def test_predict0():
    data = {'data': {'values': [4.9, 3.0, 1.4, 0.2]}}
    assert predict(data).get('result', {}).get('prediction') == '[0]'


def test_predict1():
    data = {'data': {'values': [5.9, 3.2, 4.8, 1.8]}}
    assert predict(data).get('result', {}).get('prediction') == '[1]'


def test_predict2():
    data = {'data': {'values': [6.7, 3.1, 5.6, 2.4]}}
    assert predict(data).get('result', {}).get('prediction') == '[2]'
