FROM python:3.7

ENV KAFKA_BOOTSTRAP_SERVERS "kafka:9094"

COPY src/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY src/. .

CMD ["python","-u","main.py"]


