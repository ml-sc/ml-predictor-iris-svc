import pathlib

import joblib
from sklearn import svm
from sklearn.datasets import load_iris

X, y = load_iris(return_X_y=True)
clf = svm.SVC()
clf = clf.fit(X, y)
joblib.dump(clf, pathlib.Path(__file__).parent / 'data' / 'model.pkl', compress=9)


def p(x):
    return ','.join(str(f) for f in x)


print(p(X[1]))
print(p(X[70]))
print(p(X[140]))
